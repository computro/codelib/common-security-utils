package za.co.computro.codelib.security.impl;

import org.junit.Ignore;

/**
 * holds constants used for testing
 *
 * @author <a href="mailto:mogowanemt@computro.co.za" >Theresho Mogowane</a>
 */
@Ignore
public abstract class AbstractSecurityTest {

    protected static final String KEY = "D=SJD%L1D&3FDS#*";
    protected static final String NULL_VALUE = null;
    protected static final String VALID_USERNAME = "read-only-admin";
    protected static final String PLAIN_TEXT_PASSWORD = "password";
    protected static final String ENCRYPTED_CORRECT_PASSWORD = "tutNRAr/udUVTZ5L6vkxGg==";
    protected static final String ENCRYPTED_INCORRECT_PASSWORD = "PB+6C09GCpfCjBM30HBSLQ==";
}
