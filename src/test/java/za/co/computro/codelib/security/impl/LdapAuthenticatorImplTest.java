package za.co.computro.codelib.security.impl;

import static com.shazam.shazamcrest.MatcherAssert.*;
import static com.shazam.shazamcrest.matcher.Matchers.*;
import javax.naming.AuthenticationException;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import org.junit.Test;
import org.mockito.Mockito;
import za.co.computro.codelib.security.Authenticator;
import za.co.computro.codelib.security.exception.EncryptionException;
import za.co.computro.codelib.security.vo.AuthenticatedUser;
import za.co.computro.codelib.security.vo.User;

/**
 *
 * @author <a href="mailto:mogowanemt@computro.co.za" >Theresho Mogowane</a>
 */
public class LdapAuthenticatorImplTest extends AbstractSecurityTest {

    /**
     *
     * @throws za.co.computro.codelib.security.exception.EncryptionException
     * @throws Exception
     */
    @Test(expected = AuthenticationException.class)
    public void authenticate_should_throw_exception_on_authentication_failier()
            throws EncryptionException, Exception {

        // Setup
        Authenticator authentication = new LdapAuthenticatorImpl();

        User user = new User();
        user.setUsername(VALID_USERNAME);
        user.setPassword(ENCRYPTED_INCORRECT_PASSWORD);

        Mockito.when(authentication.authenticate(user, KEY)).thenThrow(AuthenticationException.class);

        // Excersise
        authentication.authenticate(user, KEY);
    }

    /**
     * Test that the developer message is not the same as the user friendly
     * message
     *
     * @throws Exception
     */
    @Test
    public void authenticate_should_throw_exception_with_developer_message_on_encryption_failier()
            throws Exception {

        // Setup
        Authenticator authentication = new LdapAuthenticatorImpl();
        User user = new User();
        user.setUsername(VALID_USERNAME);
        user.setPassword(NULL_VALUE);

        // Excersise
        try {
            authentication.authenticate(user, KEY);
        } catch (EncryptionException ae) {
            assertNotSame("Developer message should not be the same as user " +
                    "friendly message", ae.getDeveloperMessage(), ae.getMessage());
        }
    }

    /**
     * Should through an encryption error due to null value for password
     *
     * @throws EncryptionException
     * @throws Exception
     */
    @Test(expected = EncryptionException.class)
    public void authenticate_should_throw_exception_on_decryption_failier()
            throws EncryptionException, Exception {

        // Setup
        Authenticator authenticator = new LdapAuthenticatorImpl();
        User user = new User();
        user.setUsername(VALID_USERNAME);
        user.setPassword(NULL_VALUE);

        // Excersise
        authenticator.authenticate(user, KEY);
    }

    /**
     * Tests a positive authentication scenario
     *
     * @throws EncryptionException
     * @throws Exception
     */
    @Test
    public void authenticate_should_authenticate_user_successfully()
            throws EncryptionException, Exception {

        // Setup
        Authenticator authentication = new LdapAuthenticatorImpl();
        User user = new User();

        user.setUsername(VALID_USERNAME);
        user.setPassword(ENCRYPTED_CORRECT_PASSWORD);

        AuthenticatedUser expectedResult = new AuthenticatedUser();
        expectedResult.setUsername(user.getUsername());
        expectedResult.setDisplayName(user.getUsername());

        // Excersise
        AuthenticatedUser actualResult = authentication.authenticate(user, KEY);

        // Verify
        assertThat("Authentication has passed but did not return the expected user",
                actualResult, sameBeanAs(expectedResult));
        assertThat(actualResult.getUsername(), is(user.getUsername()));
    }
}
