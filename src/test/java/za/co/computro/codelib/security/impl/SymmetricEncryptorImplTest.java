package za.co.computro.codelib.security.impl;

import static com.shazam.shazamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import org.junit.Test;
import za.co.computro.codelib.security.SymmetricEncryptor;
import za.co.computro.codelib.security.exception.EncryptionException;

/**
 *
 * @author <a href="mailto:mogowanemt@computro.co.za" >Theresho Mogowane</a>
 */
public class SymmetricEncryptorImplTest extends AbstractSecurityTest {

    /**
     *
     * @throws Exception
     */
    @Test
    public void encryptPassword_should_return_encrypted_value_successfully() throws Exception {
        // Setup
        SymmetricEncryptor encryptor = new SymmetricEncryptorImpl();

        // Exercise
        String encryptedValue = encryptor.encryptValue(KEY, PLAIN_TEXT_PASSWORD);

        // Asserts
        assertThat(encryptedValue, not(AbstractSecurityTest.PLAIN_TEXT_PASSWORD));
    }

    /**
     *
     * @throws Exception
     */
    @Test(expected = EncryptionException.class)
    public void testEncryptPassword_should_throw_exception_on_invalid_value() throws Exception {
        // Setup
        SymmetricEncryptor encryptor = new SymmetricEncryptorImpl();

        // Exercise
        encryptor.encryptValue(KEY, NULL_VALUE);
    }

    /**
     *
     * @throws Exception
     */
    @Test(expected = EncryptionException.class)
    public void encryptPassword_should_throw_exception_on_invalid_key() throws Exception {
        // Setup
        SymmetricEncryptor encryptor = new SymmetricEncryptorImpl();

        // Exercise
        encryptor.encryptValue(NULL_VALUE, PLAIN_TEXT_PASSWORD);
    }

    /**
     *
     * @throws Exception
     */
    @Test(expected = EncryptionException.class)
    public void decryptPassword_should_throw_exception_on_error() throws Exception {
        // Setup
        SymmetricEncryptor encryptor = new SymmetricEncryptorImpl();

        // Exercise
        encryptor.decryptValue(NULL_VALUE, PLAIN_TEXT_PASSWORD);
    }

    /**
     *
     * @throws Exception
     */
    @Test
    public void decryptPassword_should_return_decrypted_value_successfully() throws Exception {
        // Setup
        SymmetricEncryptor encryptor = new SymmetricEncryptorImpl();

        // Exercise
        String decryptedValue = encryptor.decryptValue(KEY, ENCRYPTED_CORRECT_PASSWORD);

        // Asserts
        assertThat(decryptedValue, is(PLAIN_TEXT_PASSWORD));
    }
}
