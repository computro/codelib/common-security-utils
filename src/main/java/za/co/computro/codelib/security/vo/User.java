package za.co.computro.codelib.security.vo;

/**
 * Hold authentication credentials (username and password)
 *
 * @author <a href="mailto:mogowanemt@computro.co.za" >Theresho Mogowane</a>
 */
public class User {

    private String username;
    private String password;

    /**
     * username getter
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * username setter
     *
     * @param username username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * password getter
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * password setter
     *
     * @param password password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }
}
