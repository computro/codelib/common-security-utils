package za.co.computro.codelib.security.impl;

import za.co.computro.codelib.security.AbstractLdapAuthenticator;
import za.co.computro.codelib.security.exception.EncryptionException;

/**
 *
 * @author <a href="mailto:mogowanemt@computro.co.za" >Theresho Mogowane</a>
 */
public class LdapAuthenticatorImpl extends AbstractLdapAuthenticator {

    /**
     * Decrypts the password using a given key. Encryption algorithm - AES
     *
     * @param secretKey the private key (salt) in multiples of 16 bytes
     * @param encryptedPassword the encrypted password to decrypt
     * @return decrypted value
     * @throws za.co.computro.codelib.security.exception.EncryptionException
     * encryption/decryption
     */
    @Override
    protected String decryptPassword(String secretKey, String encryptedPassword) throws EncryptionException {
        if (encryptedPassword == null) {
            throw new EncryptionException("Password not provided", "The password value to decrypt is NULL");
        }
        return new SymmetricEncryptorImpl().decryptValue(secretKey, encryptedPassword);
    }
}
