package za.co.computro.codelib.security.exception;

/**
 * Exception thrown when an encryption / decryption error occurs
 *
 * @author <a href="mailto:mogowanemt@computro.co.za" >Theresho Mogowane</a>
 */
public class EncryptionException extends CodeLibraryException {

    /**
     * Creates an instance of an encryption exception
     *
     * @param userFriendlyMessage user friendly message
     * @param developerMessage developer details (e.g. stack trace)
     * @see CodeLibraryException
     */
    public EncryptionException(String userFriendlyMessage, String developerMessage) {
        super(userFriendlyMessage, developerMessage);
    }

    /**
     * Creates an instance of an encryption exception wrapping another
     * exception/error that occurred
     *
     * @param message user friendly error message
     * @param cause the actual error/exception to wrap
     * @see CodeLibraryException
     */
    public EncryptionException(String message, Throwable cause) {
        super(message, cause);
    }
}
