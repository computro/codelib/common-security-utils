package za.co.computro.codelib.security;

import za.co.computro.codelib.security.exception.EncryptionException;

/**
 * Defines a symmetric encryption interface using a provided secret key
 *
 * @author <a href="mailto:mogowanemt@computro.co.za" >Theresho Mogowane</a>
 */
public interface SymmetricEncryptor {

    String decryptValue(String key, String encryptedValue) throws EncryptionException;

    String encryptValue(String key, String value) throws EncryptionException;

}
