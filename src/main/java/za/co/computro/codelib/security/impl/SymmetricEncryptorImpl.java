package za.co.computro.codelib.security.impl;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;
import za.co.computro.codelib.security.SymmetricEncryptor;
import za.co.computro.codelib.security.exception.EncryptionException;

/**
 * Implements an AES symmetric encryption using a provided secret key
 *
 * @author <a href="mailto:mogowanemt@computro.co.za" >Theresho Mogowane</a>
 */
public class SymmetricEncryptorImpl implements SymmetricEncryptor {

    /*
    The encryption algorithm: no-padding
     */
    private final String ENCRYPTION_METHOD = "AES";
    private final String CHARSET_NAME = "UTF-8";

    /**
     * Encrypts a given string value using the secret key provided
     *
     * @param secretKey the private key (salt) in multiples of 16 bytes
     * @param value the string value to encrypt
     * @return encrypted value
     * @throws za.co.computro.codelib.security.exception.EncryptionException thrown when an error occurs during
     * encryption/decryption
     */
    @Override
    public String encryptValue(String secretKey, String value) throws EncryptionException {
        try {
            if (secretKey == null || value == null) {
                throw new EncryptionException("please supply enough parameters for encryption",
                        "Please supply both secret key and value to encrypt");
            }

            byte[] encryptText = value.getBytes(CHARSET_NAME);
            byte[] key = secretKey.getBytes(CHARSET_NAME);

            SecretKeySpec skeySpec = new SecretKeySpec(key, ENCRYPTION_METHOD);
            Cipher cipher = Cipher.getInstance(ENCRYPTION_METHOD);
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec);

            String encrypted = Base64.encodeBase64String(cipher.doFinal(encryptText));

            return encrypted;

        } catch (Exception ex) {
            throw new EncryptionException("An encryption error occured", ex);
        }
    }

    /**
     * Decrypts the value using the secret key provided (symmetric).
     *
     * @param secretKey the private key (salt) in multiples of 16 bytes
     * @param encryptedValue the encrypted string value to decrypt
     * @return decrypted value
     * @throws EncryptionException thrown when an error occurs during
     * encryption/decryption
     */
    @Override
    public String decryptValue(String secretKey, String encryptedValue) throws EncryptionException {
        try {
            byte[] key = secretKey.getBytes(CHARSET_NAME);
            byte[] encryptText = Base64.decodeBase64(encryptedValue);

            SecretKeySpec skeySpec = new SecretKeySpec(key, ENCRYPTION_METHOD);
            Cipher cipher = Cipher.getInstance(ENCRYPTION_METHOD);
            cipher.init(Cipher.DECRYPT_MODE, skeySpec);

            String decryptedValue = new String(cipher.doFinal(encryptText));

            return decryptedValue;

        } catch (Exception ex) {
            throw new EncryptionException("An encryption error occured", ex);
        }
    }
}
