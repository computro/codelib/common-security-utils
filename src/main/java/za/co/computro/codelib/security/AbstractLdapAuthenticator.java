package za.co.computro.codelib.security;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import za.co.computro.codelib.security.exception.EncryptionException;
import za.co.computro.codelib.security.vo.AuthenticatedUser;
import za.co.computro.codelib.security.vo.User;

/**
 * A basic Ldap authentication class using a form of encryption method
 *
 * @author <a href="mailto:mogowanemt@computro.co.za" >Theresho Mogowane</a>
 */
public abstract class AbstractLdapAuthenticator implements Authenticator {

    public static final String LDAP_DN = "dc=example,dc=com";
    public static final String LDAP_PROVIDER_URL = "ldap://ldap.forumsys.com:389";
    public static final String LDAP_CONTEXT_FACTORY_CLASS = "com.sun.jndi.ldap.LdapCtxFactory";

    /**
     * Abstract encryption method called prior to authentication
     *
     * @param secretKey the private key (salt) in multiples of 16 bytes
     * @param password the encrypted password to decrypt
     * @return decrypted password
     * @throws za.co.computro.codelib.security.exception.EncryptionException
     * encryption/decryption
     */
    protected abstract String decryptPassword(String secretKey, String password)
            throws EncryptionException;

    /**
     * Authenticates user using an encrypted password. The secret key is also
     * provided for decryption. Returns the user's display name and username
     *
     * @param user the user to be authenticated
     * @param secretKey the private key (salt) in multiples of 16 bytes
     * @return the authenticated user details
     * @throws NamingException thrown when ldap context is not found
     * @throws EncryptionException thrown on encryption/decryption failure
     * @throws AuthenticationException thrown on authentication failure
     */
    @Override
    public AuthenticatedUser authenticate(User user, String secretKey)
            throws NamingException, EncryptionException, AuthenticationException {

        DirContext context = getInitialDirContext(user.getUsername(), decryptPassword(secretKey, user.getPassword()));
        AuthenticatedUser authenticatedUser = new AuthenticatedUser();
        authenticatedUser.setUsername(user.getUsername());

        SearchControls searchControls = setupSearchControls();

        String filter = "(&(cn=" + user.getUsername() + "))";

        NamingEnumeration<SearchResult> searchResult = context.search(LDAP_DN,
                filter, searchControls);

        // We expect one user account to match the criteria
        if(searchResult.hasMore()) {
            SearchResult result = searchResult.next();
            if(result.getAttributes().size() > 1) {
                String displayName = (String) result.getAttributes().get("displayName").get();
                authenticatedUser.setDisplayName(displayName);
            } else {
                authenticatedUser.setDisplayName(user.getUsername());
            }
        }

        return authenticatedUser;
    }

    /*
     * Sets up Ldap search controls
     */
    protected SearchControls setupSearchControls() {
        SearchControls ctls = new SearchControls();
        List<String> attrIDs = new ArrayList<>();
        attrIDs.add("displayName");
        attrIDs.add("sAMAccountName");

        ctls.setReturningAttributes(attrIDs.toArray(new String[0]));
        ctls.setCountLimit(1);
        ctls.setTimeLimit(1);
        // set to true in order to get Java objects in the NamingEnumeration
        // set to false, the search is faster, but it returns only the object name and class, not the object data
        ctls.setReturningObjFlag(true);
        // searches can either traverse links, looking for matches to a search, or ignore them
        ctls.setDerefLinkFlag(true);
        ctls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        return ctls;
    }

    /**
     * Performs the actual AD authentication for service account in a specific ldap
     * directory - override to configure otherwise
     *
     * @param username AD username (i.e. cn)
     * @param password corresponding password to verify credentials
     * @return ldap context
     * @throws NamingException thrown when ldap context is not found
     */
    @SuppressWarnings("absolete")
    protected LdapContext getInitialDirContext(String username, String password) throws NamingException {
        // Set up the environment for creating the initial context
        final Hashtable<String, Object> env = new Hashtable<>();

        env.put(Context.INITIAL_CONTEXT_FACTORY, LDAP_CONTEXT_FACTORY_CLASS);
        env.put(Context.PROVIDER_URL, LDAP_PROVIDER_URL);

        // The authentication method
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        // AD principal information corma delimeted: cn=[Username] is required
        env.put(Context.SECURITY_PRINCIPAL, "cn=" + username
                + "," + LDAP_DN);
        // User password
        env.put(Context.SECURITY_CREDENTIALS, password);
        env.put(Context.REFERRAL, "follow");

        // Authenticates the user - returns Ldap context or throws AuthenticationException repectively
        return new InitialLdapContext(env, null);
    }
}
