package za.co.computro.codelib.security;

import javax.naming.NamingException;

import za.co.computro.codelib.security.exception.EncryptionException;
import za.co.computro.codelib.security.vo.AuthenticatedUser;
import za.co.computro.codelib.security.vo.User;

/**
 *
 * @author <a href="mailto:mogowanemt@computro.co.za" >Theresho Mogowane</a>
 */
public interface Authenticator {

    /**
     *
     * @param user the user to be authenticated
     * @param privateKey the private key (salt) in multiples of 16 bytes
     * @return the authenticated user details
     * @throws NamingException thrown when ldap context is not found
     * @throws za.co.computro.codelib.security.exception.EncryptionException
     */
    AuthenticatedUser authenticate(User user, String privateKey) throws NamingException, EncryptionException;
}
