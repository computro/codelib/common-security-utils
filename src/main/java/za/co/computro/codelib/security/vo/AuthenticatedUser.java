package za.co.computro.codelib.security.vo;

/**
 * This class holds details of the Ldap user authenticated.
 *
 * @author <a href="mailto:mogowanemt@computro.co.za" >Theresho Mogowane</a>
 */
public class AuthenticatedUser {

    private String username;
    private String displayName;

    /**
     * username getter
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * username setter
     *
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * display name getter
     *
     * @return the display name
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * display name setter
     *
     * @param displayName the display name to set
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * method to represent the state of the object as a string
     *
     * @return the state of the object
     */
    @Override
    public String toString() {
        return "AuthenticatedUser{" + "username=" + username + ", displayName=" + displayName + '}';
    }
}
