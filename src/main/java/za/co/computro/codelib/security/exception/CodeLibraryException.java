package za.co.computro.codelib.security.exception;

/**
 * This exception class defines all exceptions thrown by the code library.
 * Implements user friendly and developer messages
 *
 * @author <a href="mailto:mogowanemt@computro.co.za" >Theresho Mogowane</a>
 */
public class CodeLibraryException extends RuntimeException {

    private final String developerMessage;

    /**
     *
     * @param userFriendlyMessage user friendly error message
     * @param developerMessage error details for a developer
     */
    public CodeLibraryException(String userFriendlyMessage, String developerMessage) {
        super(userFriendlyMessage);
        this.developerMessage = developerMessage;
    }

    /**
     * Constructs a new Code Library Exception with default developer message
     *
     * @param message user friendly error message
     * @param cause the actual error/exception to wrap
     */
    public CodeLibraryException(String message, Throwable cause) {
        super(message, cause);
        this.developerMessage = cause.toString();
    }

    /**
     * Gets the developer message
     *
     * @return error details for a developer
     */
    public String getDeveloperMessage() {
        return developerMessage;
    }
}
